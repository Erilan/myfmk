/*
 CATALOGUE a faire
*/

CREATE TABLE Pages (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    nom TEXT,
    contenu TEXT
)

CREATE TABLE Vignettes (
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    type TEXT,
    nom TEXT,
    titre TEXT,
    contenu TEXT
)