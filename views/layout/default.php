<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Title</title>
    <?php
    foreach($styles as $style)
        echo "<link href='".WEBROOT."ressources/css/$style.css' rel='stylesheet' type='text/css' media='screen'/>\n        ";
    foreach($scripts as $script)
        echo "<script src='".WEBROOT."ressources/js/$script.js'></script>";
    ?>
    <!--[if IE]>
    <link rel="stylesheet" href="<?php echo WEBROOT ?>ressources/css/style_ie.css" />
    <![endif]-->

</head>
<body>
    <h1>Hello world layout</h1>
    <?php
        include_once($view);
    ?>
</body>
</html>