<?php

//define root
define('WEBROOT', str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
define('ROOT', str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));

//include kernel's classes
require(ROOT.'kernel/Entity.php');
require(ROOT.'kernel/EntityCollection.php');
require(ROOT.'kernel/Controller.php');
require(ROOT.'kernel/FmkUtils.php');
require(ROOT.'kernel/Router.php');

//include the bd connection
require_once('kernel/DBConnection.php');

$router = new Router();
if($router->routeExist())
    $router->call();
else
    require(ROOT.'404.php');