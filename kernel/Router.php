<?php

require(ROOT.'kernel/spyc.php');

class Router {

    private $controller;

    private $action;

    private $parameters=null;

    private $routeExist = false;

    public function __construct(){
        $request = str_replace(WEBROOT, '', $_SERVER['REQUEST_URI']);
        if($request){
            $routes = Spyc::YAMLLoad(ROOT.'routes.yml');
            foreach($routes as $routeIterator){
                $patternToIsoleParams = $routeIterator['pattern'];
                $routeIterator['pattern'] = str_replace('/', '\/', $routeIterator['pattern']);
                if(isset($routeIterator['params'])){
                    foreach($routeIterator['params'] as $key => $type){
                        if($type=='integer')
                            $routeIterator['pattern'] = str_replace($key, '\d+', $routeIterator['pattern']);
                        if($type=='string')
                            $routeIterator['pattern'] = str_replace($key, '\w+', $routeIterator['pattern']);
                        end($routeIterator['params']);
                        if($key != key($routeIterator['params']))
                            $key .= '/';
                        $patternToIsoleParams = str_replace($key, '', $patternToIsoleParams);
                    }
                }
                $paramsAsString = str_replace($patternToIsoleParams, '', $request);
                $this->parameters = explode('/', $paramsAsString);
                if(preg_match('/^'.$routeIterator['pattern'].'$/', $request)){
                    $this->controller = $routeIterator['controller'].'Controller';
                    $this->action = $routeIterator['action'];
                    $this->routeExist = true;
                    break;
                }
            }
        } else {
            $this->controller='homeController';
            $this->action='index';
            $this->routeExist = true;
        }
    }

    public function routeExist(){
        return $this->routeExist;
    }

    public function call(){
        if(!include(ROOT . "controllers/$this->controller.php"))
            header('Location: '.WEBROOT.'404.php');
        $controllerToCall = new $this->controller();
        if(method_exists($controllerToCall, $this->action)){
            $controllerToCall->beforeAction();
            if($this->parameters != null)
                call_user_func_array(array($controllerToCall, $this->action), $this->parameters);
            else
                call_user_func(array($controllerToCall, $this->action));
            $controllerToCall->afterAction();
        } else
            header('Location: '.WEBROOT.'404.php');
    }
}