<?php
class DBConnection {

    static private $instance = null;

    public static function getInstance() {
        if (self::$instance == null) {
            $host = FmkUtils::getIniParam('DBHost');
            $base = FmkUtils::getIniParam('DBBase');
            $user = FmkUtils::getIniParam('DBUser');
            $password = FmkUtils::getIniParam('DBPassword');
            $dsn = sprintf("mysql:host=%s;dbname=%s", $host, $base);
            self::$instance = new PDO($dsn, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$instance;
    }
}
