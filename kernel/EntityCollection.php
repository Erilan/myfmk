<?php

class EntityCollection {

    public $collection = array();

    private $reflector;

    private $tableName;

    public function __construct(){
        $this->tableName = str_replace('Collection', '', get_class($this));
        $this->reflector = new ReflectionClass($this->tableName);
    }

    public function fetchAll() {
        $connection = DBConnection::getInstance();
        $stmt = $connection->prepare('SELECT * FROM '.$this->tableName.' ;');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        foreach($result as $line){
            $temp = new $this->tableName();
            foreach($line as $fieldName=>$value){
                $property = $this->reflector->getProperty($fieldName);
                $property->setAccessible(true);
                $property->setValue($temp, $value);
                $property->setAccessible(false);
            }
            $this->collection[] = $temp;
        }
    }

    public function fetchAllBy($fetchParams) {
        $connection = DBConnection::getInstance();
        $request = 'SELECT * FROM '.$this->tableName.' WHERE ';
        foreach($fetchParams as $columnName => $value){
            $request .= $columnName.' = "' . addslashes($value).'"';
            if(end($fetchParams)==$value)
                $request .= ';';
            else
                $request .= ' AND ';
        }
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $result= $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if($result){
            foreach($result as $line){
                $temp = new $this->tableName();
                foreach($line as $fieldName=>$value){
                    $property = $this->reflector->getProperty($fieldName);
                    $property->setAccessible(true);
                    $property->setValue($temp, $value);
                    $property->setAccessible(false);
                }
                $this->collection[] = $temp;
            }
        }
    }
}

?>
