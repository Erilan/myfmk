<?php

class Entity {

    private $fieldsName = array();

    private $reflector;

    protected $id;

    public function __construct(){
        $this->reflector = new ReflectionClass(get_class($this));
        $connection = DBConnection::getInstance();
        $stmt = $connection->prepare('DESCRIBE '.get_class($this));
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        foreach($result as $field){
            $this->fieldsName[] = $field['Field'];
        }
    }


    public function fetchById($id) {
        $connection = DBConnection::getInstance();
        $stmt = $connection->prepare('SELECT * FROM '.get_class($this).' WHERE id = :id ;');
        $stmt->execute(array(':id' => $id));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if($result){
            foreach($result as $fieldName => $value){
                if($fieldName != 'id'){
                    $property = $this->reflector->getProperty($fieldName);
                    $property->setAccessible(true);
                    $property->setValue($this, $value);
                    $property->setAccessible(false);
                } else
                    $this->id = $value;
            }
        }
    }


    public function fetchBy($fetchParams) {
        $connection = DBConnection::getInstance();
        $request = 'SELECT * FROM '.get_class($this).' WHERE ';
        foreach($fetchParams as $columnName => $value){
            $request .= $columnName.' = "' . addslashes($value).'"';
            if(end($fetchParams)==$value)
                $request .= ';';
            else
                $request .= ' AND ';
        }
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $result= $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        if($result){
            foreach($result as $fieldName => $value){
                if($fieldName != 'id'){
                    $property = $this->reflector->getProperty($fieldName);
                    $property->setAccessible(true);
                    $property->setValue($this, $value);
                    $property->setAccessible(false);
                } else
                    $this->id = $value;
            }
        }
    }

    public function save(){
        if($this->id == null)
            $this->create();
        else
            $this->update();
    }

    private function create(){
        $connection = DBConnection::getInstance();
        $request = 'INSERT INTO ' . get_class($this) . ' ( ';
        $values='';
        foreach($this->fieldsName as $field){
            if($field != 'id'){
                $request .= $field.',';
                $property = $this->reflector->getProperty($field);
                $property->setAccessible(true);
                $values .= '"'.$property->getValue($this).'",';
                $property->setAccessible(false);
            }
        }
        $request = substr($request,0,-1);
        $values = substr($values,0,-1);
        $request .= ') VALUES ( '.$values.' );';
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $stmt->closeCursor();
        $this->id = $connection->lastInsertId();
    }

    private function update() {
        $connection = DBConnection::getInstance();
        $request = 'UPDATE ' . get_class($this) .' SET ';
        foreach($this->fieldsName as $field){
            if($field != 'id'){
                $request .= $field;
                $property = $this->reflector->getProperty($field);
                $property->setAccessible(true);
                $request .= ' = "'.$property->getValue($this).'",';
                $property->setAccessible(false);
            }
        }
        $request = substr($request,0,-1);
        $request .= ' WHERE id = '.$this->id.' ;';
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $stmt->closeCursor();
    }

    public function delete(){
        $connection = DBConnection::getInstance();
        $stmt = $connection->prepare('DELETE FROM '.get_class($this).' WHERE id = :id ;');
        $stmt->execute(array(':id' => $this->id));
        $stmt->closeCursor();
    }

    public function getId(){
        return $this->id;
    }


}

?>
