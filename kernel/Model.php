<?php

class Model {

    private $tableName;

    function __construct($tablename){
        $this->tableName = $tablename;
    }

    public function fetchAll() {
        $connection = DBConnection::getInstance();
        $stmt = $connection->prepare('SELECT * FROM '.$this->tableName.' ;');
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $result;
    }

    public function fetchById($id) {
        $connection = DBConnection::getInstance();
        $stmt = $connection->prepare('SELECT * FROM '.$this->tableName.' WHERE id = :id ;');
        $stmt->execute(array(':id' => $id));
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->closeCursor();
        return $result;
    }
    
    public function fetchBy($fetchParams) {
        $connection = DBConnection::getInstance();
        $request = 'SELECT * FROM '.$this->tableName.' WHERE ';
        foreach($fetchParams as $columnName => $value){
            $request .= $columnName.' = ' . addslashes($value);
            if(end($fetchParams)==$value)
                $request .= ';';
            else
                $request .= ' AND ';
        }
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $resultat = $stmt->fetchAll(PDO::FETCH_ASSOC);        
        $stmt->closeCursor();
        return $resultat;
    }

    public function insert($datas) {
        $connection = DBConnection::getInstance();
        $values='';
        $request = 'INSERT INTO ' . $this->tableName . ' ( ';
        foreach($datas as $n => $v) {
            $request .= $n.',';
            $values .= '"'.$v.'",';
        }
        $request = substr($request,0,-1);
        $values = substr($values,0,-1);
        $request .= ') VALUES ( '.$values.' );';
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $stmt->closeCursor();
    }

    public function deleteById($id){
        $connection = DBConnection::getInstance();
        $stmt = $connection->prepare('DELETE FROM '.$this->tableName.' WHERE id = :id ;');
        $stmt->execute(array(':id' => $id)); 
        $stmt->closeCursor();
    }

    public function deleteBy($fetchParams) {
        $connection = DBConnection::getInstance();
        $request = 'DELETE FROM '.$this->tableName.' WHERE ';
        foreach($fetchParams as $columnName => $value){
            $request .= $columnName.' = ' . addslashes($value);
            if(end($fetchParams)==$value)
                $request .= ';';
            else
                $request .= ' AND ';
        }
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $stmt->closeCursor();
    }

    public function update($id, $datas) {
        $connection = DBConnection::getInstance();
        $request = 'UPDATE ' . $this->tableName .' SET ';
        foreach($datas as $n => $v) {
            $request .= $n.'="'.addslashes($v).'",';
        }
        $request = substr($request,0,-1);
        $request .= ' WHERE id = '.$id.' ;';
        $stmt = $connection->prepare($request);
        $stmt->execute();
        $stmt->closeCursor();
    }
    
}

?>
