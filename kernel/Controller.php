<?php

class Controller {

    public $datas = array();
    public $layout = 'default';
    public $styles = array('style');
    public $scripts = array();

    public function setData($data) {
        $this->datas = array_merge($this->datas, $data);
    }

    public function setLayout($layoutName) {
        $this->layout = $layoutName;
    }

    public function renderView($viewName) {
        $this->setData(array(
            'styles' => $this->styles,
            'scripts' => $this->scripts,
            'view' => 'views/'.str_replace('Controller', '',get_class($this)).'/'.$viewName.'.php'
        ));
        extract($this->datas);
        require(ROOT.'views/layout/'.$this->layout.'.php');
    }

    public function beforeAction(){}

    public function afterAction(){}

}

?>
