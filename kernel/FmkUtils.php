<?php

class FmkUtils {

    public static function getIniParam($paramName){
        $iniFile = fopen(ROOT.'ini', 'r');
        $iniFound = false;
        $result='';
        while(!$iniFound && $line = fgets($iniFile)){
            $explodedLine = explode('=',$line);
            if($explodedLine[0] == $paramName){
                $result = trim($explodedLine[1]);
                $iniFound = true;
            }
        }
        fclose($iniFile);
        return $result;
    }

    public static function hasPostVariable($postVariableName){
        return isset($_POST[$postVariableName]);
    }

    public static function hasNotEmptyPostVariable($postVariableName){
        return ( (isset($_POST[$postVariableName])) && ($_POST[$postVariableName]!='') );
    }

    public static function getPostVariable($postVariableName){
        return $_POST[$postVariableName];
    }

    public static function hasGetVariable($getVariableName){
        return isset($_GET[$getVariableName]);
    }

    public static function hasNotEmptyGetVariable($getVariableName){
        return ( (isset($_GET[$getVariableName])) && ($_GET[$getVariableName]!='') );
    }

    public static function getGetVariable($getVariableName){
        return $_GET[$getVariableName];
    }

    public static function hasSessionVariable($sessionVariableName){
        return isset($_SESSION[$sessionVariableName]);
    }

    public static function hasNotEmptySessionVariable($sessionVariableName){
        return ( (isset($_SESSION[$sessionVariableName])) && ($_SESSION[$sessionVariableName]!='') );
    }

    public static function getSessionVariable($sessionVariableName){
        return $_SESSION[$sessionVariableName];
    }

    public static function loadViewFull($viewName, $parameters){
        extract($parameters);
        include(ROOT.'views/viewFull/'.$viewName.'.php');
    }

    public static function loadViewSummary($viewName, $parameters){
        extract($parameters);
        include(ROOT.'views/viewSummary/'.$viewName.'.php');
    }

} 