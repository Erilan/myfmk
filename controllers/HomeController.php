<?php
class homeController extends Controller {


    public function index() {
        $this->renderView('index');
    }

    public function demo($id, $name) {
        $this->setData(array(
            'id' => $id,
            'name' => $name,
        ));
        $this->renderView('index');
    }
    
}